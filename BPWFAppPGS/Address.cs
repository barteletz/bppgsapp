﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BPWFAppPGS
{
    public partial class Address : Form
    {

        Surname surname;
        PhoneNumber phoneNumber;
        public static string writtenAddress { get; set; }
        public Address(Surname surname)
        {
            InitializeComponent();
            this.surname = surname;
            phoneNumber = new PhoneNumber(this);
        }

        private void AddressTextBox_TextChanged(object sender, EventArgs e)
        {
            writtenAddress = AddressTextBox.Text;
        }

        private void NextToPhoneNumberButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            phoneNumber.Show();
        }

        private void BackToSurnameButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            surname.Show();
        }

    }
}
