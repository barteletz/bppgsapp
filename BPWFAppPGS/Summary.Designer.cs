﻿namespace BPWFAppPGS
{
    partial class Summary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SummaryLabel = new System.Windows.Forms.Label();
            this.BackToPhoneNumberButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SummaryLabel
            // 
            this.SummaryLabel.AutoSize = true;
            this.SummaryLabel.Location = new System.Drawing.Point(12, 9);
            this.SummaryLabel.Name = "SummaryLabel";
            this.SummaryLabel.Size = new System.Drawing.Size(48, 13);
            this.SummaryLabel.TabIndex = 19;
            this.SummaryLabel.Text = "summary";
            // 
            // BackToPhoneNumberButton
            // 
            this.BackToPhoneNumberButton.Location = new System.Drawing.Point(12, 156);
            this.BackToPhoneNumberButton.Name = "BackToPhoneNumberButton";
            this.BackToPhoneNumberButton.Size = new System.Drawing.Size(95, 23);
            this.BackToPhoneNumberButton.TabIndex = 17;
            this.BackToPhoneNumberButton.Text = "Back";
            this.BackToPhoneNumberButton.UseVisualStyleBackColor = true;
            this.BackToPhoneNumberButton.Click += new System.EventHandler(this.BackToPhoneNumberButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(127, 156);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(95, 23);
            this.ExitButton.TabIndex = 16;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // Summary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 191);
            this.Controls.Add(this.SummaryLabel);
            this.Controls.Add(this.BackToPhoneNumberButton);
            this.Controls.Add(this.ExitButton);
            this.Name = "Summary";
            this.Text = "PGSapp";
            this.Load += new System.EventHandler(this.Summary_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label SummaryLabel;
        private System.Windows.Forms.Button BackToPhoneNumberButton;
        private System.Windows.Forms.Button ExitButton;
    }
}