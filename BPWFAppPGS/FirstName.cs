﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BPWFAppPGS
{
    public partial class FirstName : Form
    {
        Surname surname;
        public static string writtenName { get; set; }
        public FirstName()
        {
            InitializeComponent();
            surname = new Surname(this);
        }

        private void NextToSurnameButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            surname.Show();
        }

        private void NameTextBox_TextChanged(object sender, EventArgs e)
        {
            writtenName = NameTextBox.Text;
        }
    }
}
