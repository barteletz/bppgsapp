﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BPWFAppPGS
{
    public partial class PhoneNumber : Form
    {
        Address address;
        Summary summary;
        public static string writtenPhoneNumber { get; set; }
        public PhoneNumber(Address address)
        {
            InitializeComponent();
            this.address = address;
        }

        private void NextToSummaryButton_Click(object sender, EventArgs e)
        {
            summary = new Summary(this);
            this.Hide();
            summary.Show();
        }

        private void BackToAddressButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            address.Show();
        }

        private void PhoneNumberTextBox_TextChanged(object sender, EventArgs e)
        {
            writtenPhoneNumber = PhoneNumberTextBox.Text;
        }
    }
}
