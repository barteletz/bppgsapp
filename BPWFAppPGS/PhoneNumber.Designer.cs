﻿namespace BPWFAppPGS
{
    partial class PhoneNumber
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PhoneNumberLabel = new System.Windows.Forms.Label();
            this.PhoneNumberTextBox = new System.Windows.Forms.TextBox();
            this.BackToAddressButton = new System.Windows.Forms.Button();
            this.NextToSummaryButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PhoneNumberLabel
            // 
            this.PhoneNumberLabel.AutoSize = true;
            this.PhoneNumberLabel.Location = new System.Drawing.Point(12, 9);
            this.PhoneNumberLabel.Name = "PhoneNumberLabel";
            this.PhoneNumberLabel.Size = new System.Drawing.Size(78, 13);
            this.PhoneNumberLabel.TabIndex = 15;
            this.PhoneNumberLabel.Text = "PhoneNumber:";
            // 
            // PhoneNumberTextBox
            // 
            this.PhoneNumberTextBox.Location = new System.Drawing.Point(12, 25);
            this.PhoneNumberTextBox.Name = "PhoneNumberTextBox";
            this.PhoneNumberTextBox.Size = new System.Drawing.Size(210, 20);
            this.PhoneNumberTextBox.TabIndex = 14;
            this.PhoneNumberTextBox.TextChanged += new System.EventHandler(this.PhoneNumberTextBox_TextChanged);
            // 
            // BackToAddressButton
            // 
            this.BackToAddressButton.Location = new System.Drawing.Point(12, 156);
            this.BackToAddressButton.Name = "BackToAddressButton";
            this.BackToAddressButton.Size = new System.Drawing.Size(95, 23);
            this.BackToAddressButton.TabIndex = 13;
            this.BackToAddressButton.Text = "Back";
            this.BackToAddressButton.UseVisualStyleBackColor = true;
            this.BackToAddressButton.Click += new System.EventHandler(this.BackToAddressButton_Click);
            // 
            // NextToSummaryButton
            // 
            this.NextToSummaryButton.Location = new System.Drawing.Point(127, 156);
            this.NextToSummaryButton.Name = "NextToSummaryButton";
            this.NextToSummaryButton.Size = new System.Drawing.Size(95, 23);
            this.NextToSummaryButton.TabIndex = 12;
            this.NextToSummaryButton.Text = "Next";
            this.NextToSummaryButton.UseVisualStyleBackColor = true;
            this.NextToSummaryButton.Click += new System.EventHandler(this.NextToSummaryButton_Click);
            // 
            // PhoneNumber
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 191);
            this.Controls.Add(this.PhoneNumberLabel);
            this.Controls.Add(this.PhoneNumberTextBox);
            this.Controls.Add(this.BackToAddressButton);
            this.Controls.Add(this.NextToSummaryButton);
            this.Name = "PhoneNumber";
            this.Text = "PGSapp";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label PhoneNumberLabel;
        public System.Windows.Forms.TextBox PhoneNumberTextBox;
        private System.Windows.Forms.Button BackToAddressButton;
        private System.Windows.Forms.Button NextToSummaryButton;
    }
}