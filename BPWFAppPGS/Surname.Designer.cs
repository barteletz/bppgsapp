﻿namespace BPWFAppPGS
{
    partial class Surname
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SurnameLabel = new System.Windows.Forms.Label();
            this.SurnameTextBox = new System.Windows.Forms.TextBox();
            this.BackToNameButton = new System.Windows.Forms.Button();
            this.NextToAddressButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SurnameLabel
            // 
            this.SurnameLabel.AutoSize = true;
            this.SurnameLabel.Location = new System.Drawing.Point(9, 9);
            this.SurnameLabel.Name = "SurnameLabel";
            this.SurnameLabel.Size = new System.Drawing.Size(52, 13);
            this.SurnameLabel.TabIndex = 7;
            this.SurnameLabel.Text = "Surname:";
            // 
            // SurnameTextBox
            // 
            this.SurnameTextBox.Location = new System.Drawing.Point(12, 25);
            this.SurnameTextBox.Name = "SurnameTextBox";
            this.SurnameTextBox.Size = new System.Drawing.Size(210, 20);
            this.SurnameTextBox.TabIndex = 6;
            this.SurnameTextBox.TextChanged += new System.EventHandler(this.SurnameTextBox_TextChanged);
            // 
            // BackToNameButton
            // 
            this.BackToNameButton.Location = new System.Drawing.Point(12, 156);
            this.BackToNameButton.Name = "BackToNameButton";
            this.BackToNameButton.Size = new System.Drawing.Size(95, 23);
            this.BackToNameButton.TabIndex = 5;
            this.BackToNameButton.Text = "Back";
            this.BackToNameButton.UseVisualStyleBackColor = true;
            this.BackToNameButton.Click += new System.EventHandler(this.BackToNameButton_Click);
            // 
            // NextToAddressButton
            // 
            this.NextToAddressButton.Location = new System.Drawing.Point(130, 156);
            this.NextToAddressButton.Name = "NextToAddressButton";
            this.NextToAddressButton.Size = new System.Drawing.Size(95, 23);
            this.NextToAddressButton.TabIndex = 4;
            this.NextToAddressButton.Text = "Next";
            this.NextToAddressButton.UseVisualStyleBackColor = true;
            this.NextToAddressButton.Click += new System.EventHandler(this.NextToAddressButton_Click);
            // 
            // Surname
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 191);
            this.Controls.Add(this.SurnameLabel);
            this.Controls.Add(this.SurnameTextBox);
            this.Controls.Add(this.BackToNameButton);
            this.Controls.Add(this.NextToAddressButton);
            this.Name = "Surname";
            this.Text = "PGSapp";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label SurnameLabel;
        private System.Windows.Forms.Button BackToNameButton;
        private System.Windows.Forms.Button NextToAddressButton;
        public System.Windows.Forms.TextBox SurnameTextBox;
    }
}