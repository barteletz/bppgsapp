﻿namespace BPWFAppPGS
{
    partial class Address
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddressLabel = new System.Windows.Forms.Label();
            this.AddressTextBox = new System.Windows.Forms.TextBox();
            this.BackToSurnameButton = new System.Windows.Forms.Button();
            this.NextToPhoneNumberButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AddressLabel
            // 
            this.AddressLabel.AutoSize = true;
            this.AddressLabel.Location = new System.Drawing.Point(12, 9);
            this.AddressLabel.Name = "AddressLabel";
            this.AddressLabel.Size = new System.Drawing.Size(48, 13);
            this.AddressLabel.TabIndex = 11;
            this.AddressLabel.Text = "Address:";
            // 
            // AddressTextBox
            // 
            this.AddressTextBox.Location = new System.Drawing.Point(12, 25);
            this.AddressTextBox.Name = "AddressTextBox";
            this.AddressTextBox.Size = new System.Drawing.Size(210, 20);
            this.AddressTextBox.TabIndex = 10;
            this.AddressTextBox.TextChanged += new System.EventHandler(this.AddressTextBox_TextChanged);
            // 
            // BackToSurnameButton
            // 
            this.BackToSurnameButton.Location = new System.Drawing.Point(12, 156);
            this.BackToSurnameButton.Name = "BackToSurnameButton";
            this.BackToSurnameButton.Size = new System.Drawing.Size(95, 23);
            this.BackToSurnameButton.TabIndex = 9;
            this.BackToSurnameButton.Text = "Back";
            this.BackToSurnameButton.UseVisualStyleBackColor = true;
            this.BackToSurnameButton.Click += new System.EventHandler(this.BackToSurnameButton_Click);
            // 
            // NextToPhoneNumberButton
            // 
            this.NextToPhoneNumberButton.Location = new System.Drawing.Point(127, 156);
            this.NextToPhoneNumberButton.Name = "NextToPhoneNumberButton";
            this.NextToPhoneNumberButton.Size = new System.Drawing.Size(95, 23);
            this.NextToPhoneNumberButton.TabIndex = 8;
            this.NextToPhoneNumberButton.Text = "Next";
            this.NextToPhoneNumberButton.UseVisualStyleBackColor = true;
            this.NextToPhoneNumberButton.Click += new System.EventHandler(this.NextToPhoneNumberButton_Click);
            // 
            // Address
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 191);
            this.Controls.Add(this.AddressLabel);
            this.Controls.Add(this.AddressTextBox);
            this.Controls.Add(this.BackToSurnameButton);
            this.Controls.Add(this.NextToPhoneNumberButton);
            this.Name = "Address";
            this.Text = "PGSapp";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label AddressLabel;
        public System.Windows.Forms.TextBox AddressTextBox;
        private System.Windows.Forms.Button BackToSurnameButton;
        private System.Windows.Forms.Button NextToPhoneNumberButton;
    }
}