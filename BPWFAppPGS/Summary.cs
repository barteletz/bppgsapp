﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BPWFAppPGS
{
    public partial class Summary : Form
    {
        PhoneNumber phoneNumber;
    
        public Summary(PhoneNumber phoneNumber)
        {
            InitializeComponent();
            this.phoneNumber = phoneNumber;
        }
       
        private void BackToPhoneNumberButton_Click(object sender, EventArgs e)
        {
            this.Close();
            phoneNumber.Show();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Goodbye!");
            Application.Exit();
        }
        
        private void Summary_Load(object sender, EventArgs e)
        {
            try
            {
                SummaryLabel.Text =
                    "Name: " + FirstName.writtenName + Environment.NewLine +
                    "Surname: " + Surname.writtenSurname + Environment.NewLine +
                    "Address: " + Address.writtenAddress + Environment.NewLine +
                    "Phone number: " + PhoneNumber.writtenPhoneNumber;

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
    }
}
