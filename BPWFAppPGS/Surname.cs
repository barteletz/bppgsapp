﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BPWFAppPGS
{
    public partial class Surname : Form
    {
        FirstName name;
        Address address;
        public static string writtenSurname { get; set; }
        public Surname(FirstName name)
        {
            InitializeComponent();
            this.name = name;
            address = new Address(this);
        }

        private void BackToNameButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            name.Show();
        }

        private void NextToAddressButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            address.Show();
        }

        private void SurnameTextBox_TextChanged(object sender, EventArgs e)
        {
            writtenSurname = SurnameTextBox.Text;
        }
    }
}
